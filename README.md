# cura-build

This repository contains build scripts used to build Cura and all dependencies from scratch.

## OS X

1. Install latest version of Xcode.
    * You may also need to run ```xcode-select --install``` for some XCode commands to be available from the command line.
2. Install [homebrew](http://brew.sh/).
3. Install CMake (```brew install cmake```).
4. On Mac OS X > 10.10, run these commands (needed to build Python packaging libraries):
```
brew update
brew install openssl
brew link openssl --force
```
5. Because Fortran is necessary: *brew install gcc* (Fortran used to be a separate installation but not anymore.  Building gcc can take over 6 hours depending on your computer.)
6. You may need to run the following to build Qt:
```
cd /Applications/Xcode.app/Contents/Developer/usr/bin/
sudo ln -s xcodebuild xcrun
```
7. Run these commands from the CuraBuild directory:
```
mkdir build
cd build
cmake ..
make
```

To debug Cura.app, you need the Console open (```open -a Console```) to see stdout and stderr.  You can filter the visible events to "Cura" in the Console Search bar.

## Windows

On Windows, the following dependencies are needed for building( not all for 32-bit builds ):

* **git for windows** (https://git-for-windows.github.io/)
  * The `git` command should be available on your `%PATH%`. Make sure that the `cmd` directory in the git for windows installation directory is on the `%PATH%` and *not* its `bin` directory, otherwise mingw32 will complain about `sh.exe` being on the path.
* **CMake** (http://www.cmake.org/)
  * Once CMake is installed make sure it is available on your `%PATH%`. Check this by running `cmake --version` in the Windows console.
* **MinGW-W64** = 5.3.0 (https://sourceforge.net/projects/mingw-w64/)
  * Once installed, its `bin` directory should be available on your `%PATH%`. Test this by running `mingw32-make --version` in the Windows console.
  * MinGW can be installed on different ways, but these were tested (without guarentees):
    * MinGW w64 installer (32-/64bit): Install the correct target architecture and make sure you choose:
      * Version := 5.3.0
      * Architecture := i686 (here for 32bit)
      * Threads := posix
      * Exception := dwarf
      * Build revision := 0
* **Python** = 3.5.2 (https://www.python.org/downloads/windows/)
  * Download Python 3.5.2 - Windows x86 web-based installer
  * Once installed, `root` directory of the installation should be available on your `%PATH%`.
  * This project supports Python 3.5.1 except for a bug in cx_Freeze.  Use Python 3.5.2 if possible.
  * You will need the latest version of pip `python -m pip install -U pip`
* **Visual C++ 2015 Build Tools (http://landinghub.visualstudio.com/visual-cpp-build-tools)**:
  Go to "custom installation" and choose:
    * Select features:
      * Windows 10 SDK 10.0.10.240
* **NSIS 3.01** (http://nsis.sourceforge.net/Main_Page)
  * This application is neeeded to create the installer.
  * You'll need to add the path to your NSIS folder to your system path. (You don't need to add NSIS/bin)
  * Be sure to include the Language files in the installation.


Make sure these dependencies are available from your path.

For 32-bit builds:

```shell
REM 32-bit
mkdir build-32
cd build-32
..\env_win32.bat
cmake -G "MinGW Makefiles" ..
mingw32-make package
```

For 64-bit builds:

```shell
REM 64-bit
mkdir build-64
cd build-64
cmake -G "MinGW Makefiles" -DBUILD_64BIT=ON ..
mingw32-make
mingw32-make package
```

## Debian/Ubuntu

* To build CuraLE you'll need the following packages to be installed:

  * For Stretch:
```
apt-get install libsm-dev libxi-dev libfontconfig1-dev libbsd-dev libxdmcp-dev libxcb1-dev libgl1-mesa-dev libgcrypt20-dev liblz4-dev liblzma-dev libselinux1-dev libsystemd-dev libdbus-1-dev libstdc++-6-dev libglib2.0-dev libc6-dev libssl1.1-dev libtinfo-dev libreadline-dev libharfbuzz-dev libxkbcommon-dev gfortran gcc uuid-dev git wget curl cmake build-essential
```
  * For Buster:
```
apt-get install libsm-dev libxi-dev libfontconfig1-dev libbsd-dev libxdmcp-dev libxcb1-dev libgl1-mesa-dev libgcrypt20-dev liblz4-dev liblzma-dev libselinux1-dev libsystemd-dev libdbus-1-dev libstdc++-8-dev libglib2.0-dev libc6-dev libssl-dev libtinfo-dev libreadline-dev libharfbuzz-dev libxkbcommon-dev gfortran gcc uuid-dev git wget curl cmake build-essential
```

* Initial build
```
git clone https://gitlab.com/lulzbot3d/cura-le/curabuild-lulzbot.git
cd curabuild-lulzbot
./build_deb_package.sh
```

* One then can run CuraLE directly from build:
```
cd build/dist
LD_LIBRARY_PATH=`pwd` ./cura-lulzbot
```

* In order to rebuild just re-run
```
./build_deb_package.sh
```
